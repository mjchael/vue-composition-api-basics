import { defineStore, acceptHMRUpdate} from 'pinia'

export const useCounterStore = defineStore({
  id: 'counter',
  state: () => ({
    count: 0,
    title: 'My Counter Title'
  }),
  actions: {
    increase(amount: number) {
      this.count+=amount;
    },
    decrement(amount: number) {
      this.count-=amount;
    }
  },
  getters: {
    oddEven() {
      if (this.count %2 === 0){
        return 'Even'
      }
      return 'Odd'
    }
  }
})

// make sure to pass the right store definition, `useAuth` in this case.
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCounterStore, import.meta.hot))
}
