import {computed, nextTick, onMounted, reactive, watch} from "vue";
const counterData = reactive({
    count: 0,
    title: 'The counter'
})
function useCounter(){


    watch(() => counterData.count, (newCount, oldCount) => {
        console.log(newCount, oldCount)

        if (newCount ===99){
            alert('99 count reached')
        }
    })

    const oddEven = computed(() => {
        if (counterData.count %2 === 0){
            return 'Even'
        }
        return 'Odd'
    })

    // const increase = (amount, e) => {
    //   counterData.count +=amount
    //   nextTick(() => {
    //     console.log('dom has updated!!! in the counter!!!')
    //   })
    // }

    const increase = async (amount: number, e: Event) => {
        counterData.count +=amount
        await nextTick()
        console.log('dom has updated!!! in the counter!!!')

    }

    const decrement = (amount: number) => {
        counterData.count-=amount
    }

    onMounted(()=>{
        console.log('do some stuffs wrt to counter')
    })


    return {
        counterData,
        oddEven,
        increase,
        decrement
    }
}

export {
    useCounter
}
