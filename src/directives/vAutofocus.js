const vAutofocus = {  // will be v-autofocus
    mounted: (el) => {
        el.focus()
    }
    // but really any other hooks would have worked here.
}

export {
    vAutofocus
}
